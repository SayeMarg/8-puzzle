import globalState from './store/index.js';
import TreeNode from './classes/treeNode.js';
import BfsSearch from './classes/bfs.js';
import IdsSearch from './classes/ids.js';
import AStart from './classes/astar.js';

// Root Node Of Tree
const rootNode = new TreeNode(
        null,
        globalState.initialPuzzle,
        globalState.initialRow,
        globalState.initialCol
    );

// Create And Run Algorithm
function runSearch(AlgorithmClass, name) {
    console.log(`${name} Search Answer:`);

    const searchObject = new AlgorithmClass(rootNode);
    searchObject.findAnswer();
    searchObject.printStepToAnswer();
}


// Choose Type Of Search For Run -> BFS, IDS
const searchType = 'ASTAR';

switch (searchType) {
    case 'BFS':
        runSearch(BfsSearch, 'BFS');
    break;
    case 'IDS':
        runSearch(IdsSearch, 'IDS');
    break;
    case 'ASTAR':
        runSearch(AStart, 'A*');
    break;
    default:
        console.log('Choose Right Search Type!');
}
