import globalState from '../store/index.js';

export default class IdsSearch {
    constructor (rootNode) {
        this.rootNode = rootNode;

        this.goalNode = null;
    }

    // Call To Find Answer
    findAnswer() {
        // Depth To Search
        let currentDepth = 0, currentNode, queue;

        // Do Search While Goal Not Found
        while (!this.goalNode) {
            queue = [
                this.rootNode
            ];

            while (queue.length) {
                // Get First Item In Queue
                currentNode = queue.shift();

                // Check Node Is Goal
                if (globalState.isGoalPuzzle(currentNode.puzzle)) {
                    this.goalNode = currentNode;
                    break;
                }

                // Add Current Node Children In First Of Queue If Child.depth <= currentDepth
                if (currentNode.depth < currentDepth) {
                    currentNode.setChildren();
                    queue.unshift(...currentNode.children);
                }
            }

            // Search For Next Depth
            currentDepth++;
        }
    }

    // To Print Path From Root To Goal
    printStepToAnswer() {
        if (this.goalNode) {
            globalState.printStepToNode(this.goalNode);
        } else {
            console.log('No Answer To Solve This Puzzle!');
        }
    }
}
