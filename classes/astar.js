import globalState from '../store/index.js';

export default class AStart {
    constructor (rootNode) {
        this.rootNode = rootNode;

        this.goalNode = null;
    }

    // Call To Find Answer
    findAnswer() {
        let currentNode, queue = [
            this.rootNode
        ];

        // Set Functionality Of Root Node
        this.rootNode.calculateFunctionality();

        while (queue.length) {
            // Soft Queue To Find Node With Minimum Functionality
            queue.sort((first, second) => first.functionality - second.functionality);

            // Get First Item In Queue -> Minumum Functionality
            currentNode = queue.shift();

            // Check Node Is Goal
            if (globalState.isGoalPuzzle(currentNode.puzzle)) {
                this.goalNode = currentNode;
                break;
            }

            // Set Functionality Of Current Node Childs And Add To Queue
            currentNode.setChildren();

            for (let item of currentNode.children) {
                item.calculateFunctionality();
            }
            queue.push(...currentNode.children);
        }
    }

    // To Print Path From Root To Goal
    printStepToAnswer() {
        if (this.goalNode) {
            globalState.printStepToNode(this.goalNode);
        } else {
            console.log('No Answer To Solve This Puzzle!');
        }
    }
}
