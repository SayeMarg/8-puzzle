import globalState from '../store/index.js';

// Node Of Puzzle Tree
export default class TreeNode {
    constructor (parentNode, puzzle, row, col) {
        this.parentNode = parentNode;

        this.puzzle = puzzle;
        // Row And Col Of Zero Element
        this.row = row;
        this.col = col;

        this.children = [];

        // Determine Node Depth
        this.depth = (this.parentNode) ? (this.parentNode.depth + 1) : 0;

        // f(n) = g(n) + h(n)
        this.functionality = null;
    }

    // Copy Node Puzzle For Create Childs
    copyPuzzle() {
        return [
            [...this.puzzle[0]],
            [...this.puzzle[1]],
            [...this.puzzle[2]]
        ]
    }

    // For Swap Two Element In Puzzle
    swapByCol(puzzle, row, oldCol, newCol) {
        const value = puzzle[row][oldCol];
        puzzle[row][oldCol] = puzzle[row][newCol];
        puzzle[row][newCol] = value;
    }
    swapByRow(puzzle, col, oldRow, newRow) {
        const value = puzzle[oldRow][col];
        puzzle[oldRow][col] = puzzle[newRow][col];
        puzzle[newRow][col] = value;
    }

    // Set Childs Of This Node
    setLeftChild() {
        if (this.col > 0) {
            const col = this.col - 1;

            const childPuzzle = this.copyPuzzle();
            this.swapByCol(childPuzzle, this.row, this.col, col);

            this.children.push(new TreeNode(this, childPuzzle, this.row, col));
        }
    }
    setRightChild() {
        if (this.col < 2) {
            const col = this.col + 1;

            const childPuzzle = this.copyPuzzle();
            this.swapByCol(childPuzzle, this.row, this.col, col);

            this.children.push(new TreeNode(this, childPuzzle, this.row, col));
        }
    }
    setUpChild() {
        if (this.row > 0) {
            const row = this.row - 1;

            const childPuzzle = this.copyPuzzle();
            this.swapByRow(childPuzzle, this.col, this.row, row);

            this.children.push(new TreeNode(this, childPuzzle, row, this.col));
        }
    }
    setDownChild() {
        if (this.row < 2) {
            const row = this.row + 1;

            const childPuzzle = this.copyPuzzle();
            this.swapByRow(childPuzzle, this.col, this.row, row);

            this.children.push(new TreeNode(this, childPuzzle, row, this.col));
        }
    }
    setChildren() {
        this.setUpChild();
        this.setRightChild();
        this.setDownChild();
        this.setLeftChild();
    }

    // Set Value Of Functionality -> depth + heuristic
    calculateFunctionality() {
        if (this.functionality === null) {
            this.functionality = this.depth + globalState.calculateHeuristic(this.puzzle);
        }
    }

    // Print Puzzle Of This Node
    printPuzzle() {
        for (let i = 0; i < 3; i++) {
            console.log(this.puzzle[i][0], this.puzzle[i][1], this.puzzle[i][2]);
        }
    }
}
