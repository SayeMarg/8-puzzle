import globalState from '../store/index.js';

export default class BfsSearch {
    constructor (rootNode) {
        this.rootNode = rootNode;

        this.goalNode = null;
    }

    // Call To Find Answer
    findAnswer() {
        let currentNode, queue = [
            this.rootNode
        ];

        while (queue.length > 0) {

            // Get First Item In Queue
            currentNode = queue.shift();

            // Check Node Is Goal
            if (globalState.isGoalPuzzle(currentNode.puzzle)) {
                this.goalNode = currentNode;
                break;
            }

            // Add Current Node Children In Queue
            currentNode.setChildren();
            queue.push(...currentNode.children);
        }
    }

    // To Print Path From Root To Goal
    printStepToAnswer() {
        if (this.goalNode) {
            globalState.printStepToNode(this.goalNode);
        } else {
            console.log('No Answer To Solve This Puzzle!');
        }
    }
}
