export default {
    // Start Puzzle And Zero Element Row, Col
    initialPuzzle: [
        [7, 2, 4],
        [5, 0, 6],
        [8, 3, 1]
    ],
    initialRow: 1,
    initialCol: 1,
    // Goal Puzzle To Find
    goalPuzzle: [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8]
    ],
    // Check That Given Puzzle Is Goal Or Not
    isGoalPuzzle(puzzle) {
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (puzzle[i][j] !== this.goalPuzzle[i][j]) {
                    return false;
                }
            }
        }

        return true;
    },
    // To Calculate Heuristic -> Number Of Fulse Position Elements
    calculateHeuristic(puzzle) {
        let heuristic = 0;

        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (puzzle[i][j] !== this.goalPuzzle[i][j]) {
                    heuristic++;
                }
            }
        }

        return heuristic;
    },
    // Print Path Root To Goal Node Recursive
    printStepToNode(goalNode) {
        if (goalNode === null) {
            return;
        }

        this.printStepToNode(goalNode.parentNode);
        goalNode.printPuzzle();

        console.log('--------------------');
    }
}
